package com.example.textdemo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Institutie extends AppCompatActivity {

    private static final String TAG = "Error!";

    //Pregatire pentru scanarea codurilor
    TextView textView;
    CodeScanner codeScanner;
    CodeScannerView codeScannerView;
    //Button mChooseBtn;
    EditText editPDFName;
    ImageButton btn_upload;

    StorageReference storageReference;
    DatabaseReference databaseReference;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userID;
    String cnp="";

    Uri mImageUri;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    //criptare date si corectura date, extragerea mai multor date

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_institutie);

        getSupportActionBar().setTitle("Institution Main Page");

        //find the ImageButton and edit text
        editPDFName = findViewById(R.id.txt_pdfNameInst);
        btn_upload = findViewById(R.id.upload_btn_inst);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference("uploadsInst/" + fAuth.getCurrentUser().getUid());

        textView = (TextView) findViewById(R.id.tv_textView);
        codeScannerView = (CodeScannerView) findViewById(R.id.scanner_view);
        codeScanner = new CodeScanner(this,codeScannerView);

        codeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String rezultat = result.getText();
                        if(rezultat.indexOf("CNP") != -1) {
                            int index = rezultat.indexOf("CNP: ");
                            String cnp = rezultat.substring(index+5, index+18);

//                            int i = 0;
//                            String partial = "";
//                            rezultat = rezultat.substring(1);
//                            while (i<10) {
//                                index = rezultat.indexOf("*");
//                                partial = partial + rezultat.substring(0, index) + "\n";
//                                rezultat = rezultat.substring(index+1);
//                                i++;
//                            }
                            rezultat = rezultat.replace('*', '\n');

                            textView.setText(rezultat);
                            editPDFName.setText(cnp);
                        } else {
                            textView.setText("ERROR! This is not an identity card!");
                        }
                    }
                });
            }
        });

        codeScannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codeScanner.startPreview();
            }
        });

        //check app level permission is granted for Camera
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            //grant the permission
            requestPermissions(new String[]{Manifest.permission.CAMERA}, 101);
        }

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPDFFile();
            }
        });

        ActivityCompat.requestPermissions(Institutie.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestForCamera();
    }

    private void requestForCamera() {
        Dexter.withActivity(this).withPermission(Manifest.permission.CAMERA).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                codeScanner.startPreview();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                Toast.makeText(Institutie.this, "Camera Permission is Required", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                permissionToken.continuePermissionRequest();
            }
        }).check();
    }

    private void selectPDFFile() {
        Intent intend = new Intent();
        intend.setType("application/pdf");
        intend.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intend,"Select PDF File"),105);
    }

    public void createMyPDF(View view){

        PdfDocument myPdfDocument = new PdfDocument();
        PdfDocument.PageInfo myPageInfo = new PdfDocument.PageInfo.Builder(300,600,1).create();
        PdfDocument.Page myPage = myPdfDocument.startPage(myPageInfo);

        Paint myPaint = new Paint();
        String myString = textView.getText().toString();

        int x = 10, y = 25;

        for(String line:myString.split("\n"))
        {
            myPage.getCanvas().drawText(line, x, y, myPaint);
            y+= myPaint.descent() - myPaint.ascent();
        }

        myPdfDocument.finishPage(myPage);
        String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/myPDFInst.pdf";
        File myFile = new File(myFilePath);

        try{
            myPdfDocument.writeTo(new FileOutputStream(myFile));
            Toast.makeText(getApplicationContext(), "File saved", Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            textView.setText("ERROR!");
        }

        myPdfDocument.close();
    }

    public void onChooseaFile(View v){
        CropImage.activity().start(Institutie.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //aici fac partea de update
        if(requestCode == 105 && resultCode == RESULT_OK && data!= null && data.getData()!=null)
        {
            updatePDFFile(data.getData());
        }
    }

    private void updatePDFFile(Uri data) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();

        StorageReference reference = storageReference.child("uploadsInst/"+ fAuth.getCurrentUser().getUid()+ "/" + System.currentTimeMillis()+ ".pdf");
        reference.putFile(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();
                while(!uri.isComplete());
                Uri url = uri.getResult();

                uploadPDF uploadPDF = new uploadPDF(editPDFName.getText().toString(), url.toString());
                databaseReference.child(databaseReference.push().getKey()).setValue(uploadPDF);
                Toast.makeText(Institutie.this,"File Uploaded",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {

                double progress = (100.00 * taskSnapshot.getBytesTransferred())/ taskSnapshot.getTotalByteCount();
                progressDialog.setMessage("Uploaded: " + (int)progress+"%");

            }
        });

    }

    public void logout(View view) {
        FirebaseAuth.getInstance().signOut(); //logout
        startActivity(new Intent(getApplicationContext(), Login.class));
        finish();
    }


    public void profile(View view) {
        startActivity(new Intent(getApplicationContext(), ProfilInst.class));
        finish();
    }
}