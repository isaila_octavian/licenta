package com.example.textdemo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Error!";
    ImageView imageView;
    EditText textView;
    //Button mChooseBtn;
    EditText editPDFName;
    ImageButton btn_upload;

    StorageReference storageReference;
    DatabaseReference databaseReference;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userID;
    String cnp="",nume="",prenume="",seria="",nr="";
    long diff = 0;

    Uri mImageUri;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    //criptare date si corectura date, extragerea mai multor date

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("User Home Page");
        //find the ImageView
        imageView = findViewById(R.id.imageId);
        //find the ImageButton and edit text
        editPDFName = findViewById(R.id.txt_pdfName);
        btn_upload = findViewById(R.id.upload_btn);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference("uploads/" + fAuth.getCurrentUser().getUid());

        //find TextView
        textView = findViewById(R.id.textId);
        //check app level permission is granted for Camera
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            //grant the permission
            requestPermissions(new String[]{Manifest.permission.CAMERA}, 101);
        }

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPDFFile();
            }
        });

        ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
    }

    private void selectPDFFile() {
        Intent intend = new Intent();
        intend.setType("application/pdf");
        intend.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intend,"Select PDF File"),105);
    }

    public void createMyPDF(View view){

        PdfDocument myPdfDocument = new PdfDocument();
        PdfDocument.PageInfo myPageInfo = new PdfDocument.PageInfo.Builder(300,600,1).create();
        PdfDocument.Page myPage = myPdfDocument.startPage(myPageInfo);

        Paint myPaint = new Paint();
        String myString = textView.getText().toString();

        int x = 10, y = 25;

        for(String line:myString.split("\n"))
        {
            myPage.getCanvas().drawText(line, x, y, myPaint);
            y+= myPaint.descent() - myPaint.ascent();
        }

        myPdfDocument.finishPage(myPage);
        String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/myPDFFile.pdf";
        File myFile = new File(myFilePath);

        try{
           myPdfDocument.writeTo(new FileOutputStream(myFile));
            Toast.makeText(getApplicationContext(), "File saved", Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            textView.setText("ERROR!");
        }

        myPdfDocument.close();

    }


    public void onChooseaFile(View v){
        CropImage.activity().start(MainActivity.this);
    }

    private void pickImageFromGallery(){
        //intent to pick image
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    //handle result of runtime permission


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_CODE:{
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //permission was granted
                    pickImageFromGallery();
                }
                else{
                    Toast.makeText(this,"Permission denied...!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //handle result of picked image


    public void doProcess(View view) {
        //open the camera => create an Intent object
        Intent intent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 101);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if(resultCode == RESULT_OK){
                mImageUri = result.getUri();
                imageView.setImageURI(mImageUri);
            }
            else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                Exception e = result.getError();
                Toast.makeText(this,"Possible error is : "+e,Toast.LENGTH_SHORT).show();
            }
            //process the image
                //1. To create a FirebaseVisionImage object from a Bitmap object:
            FirebaseVisionImage firebaseVisionImage = null;
            try {
                firebaseVisionImage = FirebaseVisionImage.fromFilePath(this.getApplicationContext(), mImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //2. Get an instance of FirebaseVision
                FirebaseVision firebaseVision = FirebaseVision.getInstance();
                //3. Create an instance of FirebaseVisionTextRecognizer
                FirebaseVisionTextRecognizer firebaseVisionTextRecognizer = firebaseVision.getOnDeviceTextRecognizer();
                //4. Create a task to process the image
                Task<FirebaseVisionText> task = firebaseVisionTextRecognizer.processImage(firebaseVisionImage);
                //5. If the task is success
                task.addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(FirebaseVisionText firebaseVisionText) {
                        String s = firebaseVisionText.getText();
                        //aici procesez Stringul extras
                        //System.out.println("Printez stringul " + s);
                        String procesare = null;
                        try {
                            procesare = procesareString(s);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        // dupa prelucrarea textului se vor tiparii informatiile filtrate
                        userID = fAuth.getCurrentUser().getUid();
                        editPDFName.setText(cnp);
                        DocumentReference documentReference = fStore.collection("users").document(userID);
                        Map<String,Object> user = new HashMap<>();
                        String finalProcesare = procesare;
                        System.out.println("Finalll " + finalProcesare);
                        documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                String fullName = documentSnapshot.getString("fName");
                                String email = documentSnapshot.getString("email");
                                String isUser = documentSnapshot.getString("isUser");

                                if((documentSnapshot.getString("CNPuser") != null && documentSnapshot.getString("CNPuser").equals(cnp)) || (documentSnapshot.getString("CNPuser") == null)){

                                    AlertDialog.Builder mydialog = new AlertDialog.Builder(MainActivity.this);
                                    mydialog.setTitle("Do you want to overwrite your data?");

                                    mydialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            user.put("fName", fullName);
                                            user.put("email", email);
                                            user.put("isUser", isUser);
                                            user.put("CNPuser", cnp);
                                            user.put("buletinUser", finalProcesare);
                                            user.put("expirare", diff);

                                            documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Log.d(TAG, "onSuccess: user Profile is created for" + userID);
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d(TAG, "onFailure: user Profile is created for" + e.toString());
                                                }
                                            });
                                            // setez textViev pe result
                                            textView.setText(finalProcesare);
                                        }
                                    });
                                    mydialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            textView.setText(finalProcesare);
                                            dialog.cancel();
                                        }
                                    });
                                    mydialog.show();

                                } else {
                                    textView.setText("Error! The ID of the Identity Card doesn't match with the last one or the image or the picture in not well taken!");
                                    Toast.makeText(MainActivity.this, "CNP nu coincide cu cel actual sau fotografia nu este facuta corect!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                });
                //6. If the task is failure
                task.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        }

        //aici fac partea de update
        if(requestCode == 105 && resultCode == RESULT_OK && data!= null && data.getData()!=null)
        {
            updatePDFFile(data.getData());
        }

    }

    private void updatePDFFile(Uri data) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();

        StorageReference reference = storageReference.child("uploads/"+ fAuth.getCurrentUser().getUid()+ "/" + System.currentTimeMillis()+ ".pdf");
        reference.putFile(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();
                while(!uri.isComplete());
                Uri url = uri.getResult();

                uploadPDF uploadPDF = new uploadPDF(editPDFName.getText().toString(), url.toString());
                databaseReference.child(databaseReference.push().getKey()).setValue(uploadPDF);
                Toast.makeText(MainActivity.this,"File Uploaded",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {

            double progress = (100.00 * taskSnapshot.getBytesTransferred())/ taskSnapshot.getTotalByteCount();
            progressDialog.setMessage("Uploaded: " + (int)progress+"%");

            }
        });

    }

    public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static int min(int... numbers) {
        return Arrays.stream(numbers)
                .min().orElse(Integer.MAX_VALUE);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    static int calculate(String x, String y) {
        int[][] dp = new int[x.length() + 1][y.length() + 1];

        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1]
                                    + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)),
                            dp[i - 1][j] + 1,
                            dp[i][j - 1] + 1);
                }
            }
        }

        return dp[x.length()][y.length()];
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String procesareString(String s) throws ParseException {

        String lowerCaseSentence = s.toLowerCase();

        final String[][] words = {s.split("\\s+")};

        String result = "";
        int diferente = 0;
        if(words[0][words[0].length-2].length()<2 || words[0][words[0].length-3].length()<2) {
            return "Error! Try to make other photo!";
        }
        String sub = words[0][words[0].length-2].substring(0,2);
        String sub1 = words[0][words[0].length-3].substring(0,2);
        if(sub.equals("ID") && words[0][words[0].length-2].length()>34) {
            nume = prelucrareNume(words[0][words[0].length-2]);
            prenume = prelucrarePrenume(words[0][words[0].length-2]);
            seria = prelucrareSerie(words[0][words[0].length-1]);
            nr = prelucrareNumar(words[0][words[0].length-1]);
        } else if(sub1.equals("ID") && words[0][words[0].length-3].length()>34) {
            nume = prelucrareNume(words[0][words[0].length-3]);
            prenume = prelucrarePrenume(words[0][words[0].length-3]);
            seria = prelucrareSerie(words[0][words[0].length-2]);
            nr = prelucrareNumar(words[0][words[0].length-2]);
        }else return "Error! Try to make other photo!";

        for (int i = 0; i < words[0].length-1; i++) {
            System.out.println(words[0][i]);

                if (words[0][i].equals("NR")) {
                    if (calculate(words[0][i + 1], nr) > 0) {
                        result = result + "* NR: " + words[0][i + 1] + "(verificare user)" + "\n";
                        Toast.makeText(getApplicationContext(), "WRONG NR!", Toast.LENGTH_LONG).show();
                        diferente++;
                    } else {
                        result = result + "* NR: " + words[0][i + 1] + "\n";
                    }
                }
                if (words[0][i].equals("CNP") || words[0][i].equals("GNP")) {
                    result = result + "* CNP: " + words[0][i + 1] + "\n";
                    cnp = words[0][i + 1];
                }
                if (words[0][i].equals("SERIA")) {
                    if(calculate(words[0][i+1], seria) > 0) {
                        result = result + "* SERIA: " + words[0][i + 1] + "(verificare user)" + "\n";
                        Toast.makeText(getApplicationContext(), "WRONG SERIE!", Toast.LENGTH_LONG).show();
                        diferente++;
                    } else {
                        result = result + "* SERIA: " + words[0][i + 1] + "\n";
                    }
                }
                if ((words[0][i].indexOf("Nume") != -1 ||  words[0][i].indexOf("Nom") != -1 || words[0][i].indexOf("Last") != -1) && words[0][i+1].equals("name")) {
                    if(calculate(words[0][i+2], nume)>0) {
                        result = result + "* NUME: " + words[0][i + 2] + ("verificare user") + "\n";
                        Toast.makeText(getApplicationContext(), "WRONG NUME!", Toast.LENGTH_LONG).show();
                        diferente++;
                    } else {
                        result = result + "* NUME: " + words[0][i + 2] + "\n";
                    }
                }
                if ((words[0][i].indexOf("Prenume") != -1 ||  words[0][i].indexOf("Prenom") != -1 || words[0][i].indexOf("First") != -1) && (words[0][i+1].equals("name") || words[0][i+1].equals("namee"))) {
                    if(words[0][i+2].indexOf("-") != -1) {
                        if(calculate(words[0][i+2], prenume) > 0) {
                            result = result + "* PRENUME: " + words[0][i + 2] + "(verificare user)" + "\n";
                            Toast.makeText(getApplicationContext(), "WRONG PRENUME!", Toast.LENGTH_LONG).show();
                            diferente++;
                        } else {
                            result = result + "* PRENUME: " + prenume + "\n";
                        }
                    } else {
                        if(calculate(words[0][i+2], prenume) > 1) {
                            result = result + "* PRENUME: " + words[0][i + 2] + "(verificare user)" + "\n";
                            Toast.makeText(getApplicationContext(), "WRONG PRENUME!", Toast.LENGTH_LONG).show();
                            diferente++;
                        } else {
                            result = result + "* PRENUME: " + prenume + "\n";
                        }
                    }
                }
                if(words[0][i].indexOf("Cetätenie") != -1 || words[0][i].indexOf("Nationalite") != -1 || words[0][i].indexOf("Nationality") != -1) {
                    int j= i+1;
                    result = result + "* CETATENIE: ";
                    char ch = words[0][j].charAt(0);
                    while(words[0][j].indexOf("Loc") == -1 && !Character.isDigit(ch)) {
                        result = result + words[0][j] + " ";
                        j++;
                        ch = words[0][j].charAt(0);
                    }
                    result = result + "\n";
                }
                if(words[0][i].indexOf("Sex") != -1 || words[0][i].indexOf("Sexe") != -1) {
                    if(cnp.startsWith("1") || cnp.startsWith("3") || cnp.startsWith("5") || cnp.startsWith("7")) {
                        result = result + "* SEX: M" + "\n";
                    } else {
                        result = result + "* SEX: F" + "\n";
                    }
                }
                if(words[0][i].indexOf("Place of birth") != -1 || words[0][i].indexOf("of birth") != -1 || words[0][i].indexOf("birth") != -1) {
                    result = result + "LOCUL DE NASTERE: " + words[0][i+1] + " " + words[0][i+2] + "\n";
                }
                if(words[0][i].indexOf("Domiciliu") != -1 || words[0][i].indexOf("Adresse") != -1 || words[0][i].indexOf("Address") != -1) {
                    int k = i+1;
                    int lungime = words[0][k].length();
                    char ch = words[0][k].charAt(lungime-1);
                    result = result + "* DOMICILIU: ";
                    while(!Character.isDigit(ch)) {
                        result = result + words[0][k] + " ";
                        k++;
                        lungime = words[0][k].length();
                        ch = words[0][k].charAt(lungime-1);
                    }
                    if(words[0][k].indexOf("nr.") != -1) {
                        result = result + words[0][k] + "\n";
                    } else if(words[0][k].indexOf("bl.") != -1) {
                        result = result + words[0][k] + " " + words[0][k+1] + " " + words[0][k+2] + "\n";
                    }

                }
                if(words[0][i].indexOf("SPCLEP") != -1) {
                    result = result + "* EMISA DE: " + words[0][i] + " " + words[0][i+1] + "\n";
                }
                if(words[0][i].indexOf("Valabilitate") != -1 || words[0][i].indexOf("Validite") != -1 || words[0][i].indexOf("Validity") != -1) {
                    char ch = words[0][i+1].charAt(0);
                    char ch2 = words[0][i+3].charAt(0);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                    System.out.println("Prima ch " + ch);
                    System.out.println("Prima ch2 " + ch2);
                    boolean ver = Character.isDigit('S');
                    boolean ver1 = Character.isDigit(ch);
                    boolean ver2 = Character.isDigit(ch2);
                    System.out.println("Verificare " + ver);
                    System.out.println("Verificare1 " + ver1);
                    System.out.println("Verificare2 " + ver2);
                    if(Character.isDigit(ch)){
                        String data1 = words[0][i+1].substring(9);
                        Date firstDate = sdf.parse(data1);
                        Date secondDate = new Date();
                        long diffInMillies = Math.abs(firstDate.getTime() - secondDate.getTime());
                        diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
                        if(diff < 0) {
                            Toast.makeText(getApplicationContext(), "Buletin Expirat!", Toast.LENGTH_LONG).show();
                            diferente++;
                        } else {
                            Toast.makeText(getApplicationContext(), "Buletin Expira in " + diff + " zile", Toast.LENGTH_LONG).show();
                            result = result + "* VALABILITATE: " + words[0][i + 1] + "\n";
                        }
                    } else if(Character.isDigit(ch2)){
                        String data1 = words[0][i+3].substring(9);
                        Date firstDate = sdf.parse(data1);
                        Date secondDate = new Date();
                        long diffInMillies = Math.abs(firstDate.getTime() - secondDate.getTime());
                        diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
                        if(diff < 0) {
                            Toast.makeText(getApplicationContext(), "Buletin Expira in " + diff + " zile", Toast.LENGTH_LONG).show();
                            diferente++;
                        } else {
                            Toast.makeText(getApplicationContext(), "Buletin Expirat!", Toast.LENGTH_LONG).show();
                            result = result + "* VALABILITATE: " + words[0][i + 3] + "\n";
                        }
                    }
                }
        }
        if(diferente>0) {
            AlertDialog.Builder mydialog = new AlertDialog.Builder(MainActivity.this);
            mydialog.setTitle("Dates doesn't match! Please check it by yourself!");

            int finalDiferente = diferente;
            mydialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getApplicationContext(), "Au fost detectate " + finalDiferente + " diferente!", Toast.LENGTH_LONG).show();
                }
            });
            mydialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getApplicationContext(), "Au fost detectate " + finalDiferente + " diferente!", Toast.LENGTH_LONG).show();
                    dialog.cancel();
                }
            });
            mydialog.show();
        }

        return result;
    }

    private String prelucrareNumar(String s) {
        String numar = s.substring(2,8);
        System.out.println("Numarul este " + numar);
        return numar;
    }

    private String prelucrarePrenume(String s) {
        System.out.println("Prima linieee " + s);
        int index = s.indexOf('<');
        String ramas = s.substring(index+2);
        int index2 = ramas.indexOf('<');
        String prenume = ramas.substring(0,index2);
        String prenumeramas = ramas.substring(index2+1);
        System.out.println("ramassss " + prenumeramas);
        char ch = prenumeramas.charAt(0);
        if(Character.isLetter(ch)) {
            int index3 = prenumeramas.indexOf('<');
            prenume = prenume + "-" + prenumeramas.substring(0, index3);
        }
        System.out.println("Prenumeles este " + prenume);
        return prenume;
    }

    private String prelucrareSerie(String s) {
        String serie = s.substring(0,2);
        System.out.println("Seria este " + serie);
        return serie;
    }

    private String prelucrareNume(String s) {
        int index = s.indexOf('<');
        String nume = s.substring(5,index);
        System.out.println("Numele este " + nume);
        return nume;
    }

    public void logout(View view) {
        FirebaseAuth.getInstance().signOut(); //logout
        startActivity(new Intent(getApplicationContext(), Login.class));
        finish();
    }

    public void profile(View view) {
        startActivity(new Intent(getApplicationContext(), Profile.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.my_menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.notification_icon);

         menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
             @Override
             public boolean onMenuItemClick(MenuItem item) {

                 userID = fAuth.getCurrentUser().getUid();
                 DocumentReference documentReference = fStore.collection("users").document(userID);
                 Map<String,Object> user = new HashMap<>();
                 documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                     @Override
                     public void onSuccess(DocumentSnapshot documentSnapshot) {
                         Long diferenta = documentSnapshot.getLong("expirare");

                         AlertDialog.Builder mydialog = new AlertDialog.Builder(MainActivity.this);
                         mydialog.setTitle("ATENTIE! Zile ramase pana la expirarea cartii de identitate de la ultima verificare: " + diferenta);

                         mydialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {

                             }
                         });
                         mydialog.show();
                     }
                 });

                 return true;
             }
         });
        return super.onCreateOptionsMenu(menu);
    }
}